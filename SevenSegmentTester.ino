// x             x
// x             x
// x-D2          x
// x-D3          x
// x-D4          x
// x-D5          x
// x-D6          x
// x-D7          x
// x-D8          x
// x-A5          x
// x-D9          x
// 1-A4      D2-18
// 2-D10     D3-17
// 3-A3      D4-16
// 4-D11     D5-15
// 5-A2      D6-14
// 6-D12     D7-13
// 7-A1      D8-12
// 8-A0      A5-11
// 9-D13     D9-10

// A1 A2  B  C D2 D1  E  F G1 G2  H  J  K  L  M  N DP
//  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17

// CC1 CC2 CC3 CC4 CC5 CC6 CC7 CC8 CC9 CC10
//  90  91  92  93  94  95  96  97  98   99

#define ZPin1 0
#define ZPin2 1
#define ZPin3 2
#define ZPin4 3
#define ZPin5 4
#define ZPin6 5
#define ZPin7 6
#define ZPin8 7
#define ZPin9 8
#define ZPin10 9
#define ZPin11 10
#define ZPin12 11
#define ZPin13 12
#define ZPin14 13
#define ZPin15 14
#define ZPin16 15
#define ZPin17 16
#define ZPin18 17

#define segA  1
#define segA1 1
#define segA2 2
#define segB  3
#define segC  4
#define segD  5
#define segD2 5
#define segD1 6
#define segE  7
#define segF  8
#define segG  9
#define segG1 9
#define segG2 10
#define segH  11
#define segJ  12
#define segK  13
#define segL  14
#define segM  15
#define segN  16
#define segDP 17

#define segCC1  90
#define segCC2  91
#define segCC3  92
#define segCC4  93
#define segCC5  94
#define segCC6  95
#define segCC7  96
#define segCC8  97
#define segCC9  98
#define segCC10 99

#include "GPIO.h"                  // https://github.com/mikaelpatel/Arduino-GPIO
#include <EEPROM.h>

bool timetoprint = false;
byte ltestno = 0;
byte slot = 0;

struct ledData_t
{
  byte Pins[18];
  byte IsCC;
}
LEDData;


void testA1A2()
{
  AllPinsInput();

  SetPin(1, 0);
  SetPin(11, 1);
  delayMicroseconds(500);
  AllPinsInput();

  SetPin(18, 0);
  SetPin(11, 1);
  delayMicroseconds(500);
  AllPinsInput();

  delay(4);
}


// 1-        -18
// 2-        -17
// 3-        -16
// 4-        -15
// 5-        -14
// 6-        -13
// 7-        -12
// 8- +      -11
// 9- -      -10


void ledTester1()
{
  SetPin(8, 1);
  SetPin(9, 0);
  delayMicroseconds(500);
  AllPinsInput();
  delay(4);
}


void initLedTester()
{
  Serial.println(F("LEDTester"));

  LEDData.Pins[ZPin1] = 0;
  LEDData.Pins[ZPin2] = 0;
  LEDData.Pins[ZPin3] = 0;
  LEDData.Pins[ZPin4] = 0;
  LEDData.Pins[ZPin5] = 0;
  LEDData.Pins[ZPin6] = 0;
  LEDData.Pins[ZPin7] = 0;
  LEDData.Pins[ZPin8] = segA;
  LEDData.Pins[ZPin9] = segCC1;
  LEDData.Pins[ZPin10] = 0;
  LEDData.Pins[ZPin11] = 0;
  LEDData.Pins[ZPin12] = 0;
  LEDData.Pins[ZPin13] = 0;
  LEDData.Pins[ZPin14] = 0;
  LEDData.Pins[ZPin15] = 0;
  LEDData.Pins[ZPin16] = 0;
  LEDData.Pins[ZPin17] = 0;
  LEDData.Pins[ZPin18] = 0;

  LEDData.IsCC = 1;
}


// 1-A1    A2-18
// 2- J    K -17
// 3- H    B -16
// 4- F    G2-15
// 5-G1    L -14
// 6- M    C -13
// 7- N    DP-12
// 8- E    CC-11
// 9-D1    D2-10


void init16segCA()
{
  Serial.println(F("16segCA"));

  LEDData.Pins[ZPin1] = segA1;
  LEDData.Pins[ZPin2] = segJ;
  LEDData.Pins[ZPin3] = segH;
  LEDData.Pins[ZPin4] = segF;
  LEDData.Pins[ZPin5] = segG1;
  LEDData.Pins[ZPin6] = segM;
  LEDData.Pins[ZPin7] = segN;
  LEDData.Pins[ZPin8] = segE;
  LEDData.Pins[ZPin9] = segD1;
  LEDData.Pins[ZPin10] = segD2;
  LEDData.Pins[ZPin11] = segCC1;
  LEDData.Pins[ZPin12] = segDP;
  LEDData.Pins[ZPin13] = segC;
  LEDData.Pins[ZPin14] = segL;
  LEDData.Pins[ZPin15] = segG2;
  LEDData.Pins[ZPin16] = segB;
  LEDData.Pins[ZPin17] = segK;
  LEDData.Pins[ZPin18] = segA2;

  LEDData.IsCC = 0;
}


void init16segCC()
{
  Serial.println(F("16segCC"));

  LEDData.Pins[ZPin1] = segA1;
  LEDData.Pins[ZPin2] = segJ;
  LEDData.Pins[ZPin3] = segH;
  LEDData.Pins[ZPin4] = segF;
  LEDData.Pins[ZPin5] = segG1;
  LEDData.Pins[ZPin6] = segM;
  LEDData.Pins[ZPin7] = segN;
  LEDData.Pins[ZPin8] = segE;
  LEDData.Pins[ZPin9] = segD1;
  LEDData.Pins[ZPin10] = segD2;
  LEDData.Pins[ZPin11] = segCC1;
  LEDData.Pins[ZPin12] = segDP;
  LEDData.Pins[ZPin13] = segC;
  LEDData.Pins[ZPin14] = segL;
  LEDData.Pins[ZPin15] = segG2;
  LEDData.Pins[ZPin16] = segB;
  LEDData.Pins[ZPin17] = segK;
  LEDData.Pins[ZPin18] = segA2;

  LEDData.IsCC = 1;
}


// 1-        -18
// 2-        -17
// 3-        -16
// 4-E    CA1-15
// 5-D      A-14
// 6-DP     F-13
// 7-C    CA2-12
// 8-G    CA3-11
// 9-X      B-10


void init3digit()
{
  Serial.println(F("3digCA"));

  LEDData.Pins[ZPin1] = 0;
  LEDData.Pins[ZPin2] = 0;
  LEDData.Pins[ZPin3] = 0;
  LEDData.Pins[ZPin4] = segE;
  LEDData.Pins[ZPin5] = segD;
  LEDData.Pins[ZPin6] = segDP;
  LEDData.Pins[ZPin7] = segC;
  LEDData.Pins[ZPin8] = segG;
  LEDData.Pins[ZPin9] = 0;
  LEDData.Pins[ZPin10] = segB;
  LEDData.Pins[ZPin11] = segCC3;
  LEDData.Pins[ZPin12] = segCC2;
  LEDData.Pins[ZPin13] = segF;
  LEDData.Pins[ZPin14] = segA;
  LEDData.Pins[ZPin15] = segCC1;
  LEDData.Pins[ZPin16] = 0;
  LEDData.Pins[ZPin17] = 0;
  LEDData.Pins[ZPin18] = 0;

  LEDData.IsCC = 0;
}


// 1-         -18
// 2-         -17
// 3-         -16
// 4-         -15
// 5-E       G-14
// 6-D       F-13
// 7-CA      X-12
// 8-C       A-11
// 9-DP      B-10


void init1digitCC()
{
  Serial.println(F("1digitCC"));

  LEDData.Pins[ZPin1] = 0;
  LEDData.Pins[ZPin2] = 0;
  LEDData.Pins[ZPin3] = 0;
  LEDData.Pins[ZPin4] = 0;
  LEDData.Pins[ZPin5] = segE;
  LEDData.Pins[ZPin6] = segD;
  LEDData.Pins[ZPin7] = segCC1;
  LEDData.Pins[ZPin8] = segC;
  LEDData.Pins[ZPin9] = segDP;
  LEDData.Pins[ZPin10] = segB;
  LEDData.Pins[ZPin11] = segA;
  LEDData.Pins[ZPin12] = 0;
  LEDData.Pins[ZPin13] = segF;
  LEDData.Pins[ZPin14] = segG;
  LEDData.Pins[ZPin15] = 0;
  LEDData.Pins[ZPin16] = 0;
  LEDData.Pins[ZPin17] = 0;
  LEDData.Pins[ZPin18] = 0;

  LEDData.IsCC = 1;
}


// 1-         -18
// 2-         -17
// 3-         -16
// 4-         -15
// 5-E       G-14
// 6-D       F-13
// 7-CA      X-12
// 8-C       A-11
// 9-DP      B-10


void init1digitCA()
{
  Serial.println(F("1digitCA"));

  LEDData.Pins[ZPin1] = 0;
  LEDData.Pins[ZPin2] = 0;
  LEDData.Pins[ZPin3] = 0;
  LEDData.Pins[ZPin4] = 0;
  LEDData.Pins[ZPin5] = segE;
  LEDData.Pins[ZPin6] = segD;
  LEDData.Pins[ZPin7] = segCC1;
  LEDData.Pins[ZPin8] = segC;
  LEDData.Pins[ZPin9] = segDP;
  LEDData.Pins[ZPin10] = segB;
  LEDData.Pins[ZPin11] = segA;
  LEDData.Pins[ZPin12] = 0;
  LEDData.Pins[ZPin13] = segF;
  LEDData.Pins[ZPin14] = segG;
  LEDData.Pins[ZPin15] = 0;
  LEDData.Pins[ZPin16] = 0;
  LEDData.Pins[ZPin17] = 0;
  LEDData.Pins[ZPin18] = 0;

  LEDData.IsCC = 0;
}


void initDual14SegmentCC()
{
  Serial.println(F("Dual14SegmentCC"));

  LEDData.Pins[ZPin1] = segE;
  LEDData.Pins[ZPin2] = segN;
  LEDData.Pins[ZPin3] = 0;
  LEDData.Pins[ZPin4] = segM;
  LEDData.Pins[ZPin5] = segL;
  LEDData.Pins[ZPin6] = segG2;
  LEDData.Pins[ZPin7] = segD;
  LEDData.Pins[ZPin8] = segDP;
  LEDData.Pins[ZPin9] = segC;

  LEDData.Pins[ZPin10] = segB;
  LEDData.Pins[ZPin11] = segCC2;
  LEDData.Pins[ZPin12] = segA;
  LEDData.Pins[ZPin13] = segG1;
  LEDData.Pins[ZPin14] = segK;
  LEDData.Pins[ZPin15] = segJ;
  LEDData.Pins[ZPin16] = segCC1;
  LEDData.Pins[ZPin17] = segH;
  LEDData.Pins[ZPin18] = segF;

  LEDData.IsCC = 1;
}

#define MaxTest 6

void initDisplay(byte routine)
{
  switch (routine)
  {
    case 0:
      {
        initLedTester();
        break;
      }
    case 1:
      {
        init1digitCC();
        break;
      }
    case 2:
      {
        init1digitCA();
        break;
      }
    case 3:
      {
        init16segCC();
        break;
      }
    case 4:
      {
        init16segCA();
        break;
      }
    case 5:
      {
        init3digit();
        break;
      }
    case 6:
      {
        initDual14SegmentCC();
        break;
      }
  }
}


void setup()
{
  // put your setup code here, to run once:

  Serial.begin(9600);
  Serial.println(F("LED Tester 02132020"));
  AllPinsInput();

  byte seltest = EEPROM.read(1);

  if (seltest > MaxTest) {
    seltest = 0;
  }

  if (EEPROM.read(2) == 0)
  {
    Serial.println(F("double reset"));
    seltest++;
    if (seltest > MaxTest) {
      seltest = 0;
    }
    EEPROM.write(1, seltest);
  }

  EEPROM.write(2, 0);

  ltestno = seltest + 1;

  initDisplay(seltest);
}


void loop()
{
  // put your main code here, to run repeatedly:

  static unsigned long ul_LastInc = millis();
  static const unsigned long ul_StepTimer = 600UL;

  static unsigned long ul_ProgramStart = millis();
  static const unsigned long ul_SaveTimer = 5000UL;
  static byte Saved = 0;

  byte inc = 0;

  if ((millis() - ul_LastInc) > ul_StepTimer)
  {
    ul_LastInc = millis();
    inc = 1;
  }

  if (((millis() - ul_ProgramStart) > ul_SaveTimer) && (Saved == 0))
  {
    Saved = 1;
    EEPROM.write(2, 1);
  }

  LighSegment(inc);

  delay(4);
}


byte IncDigit(byte inc = 0)
{
  static byte digit = 90;

  if (inc)
  {
    digit++;
    if (digit == 100)
    {
      digit = 90;
    }
  }

  return digit;
}


byte IncSegment(byte inc = 0)
{
  static byte segment = 1;

  if (inc)
  {
    segment++;
    if (segment == 18)
    {
      segment = 1;
      IncDigit(1);
    }
  }

  return segment;
}


signed char FindPin(byte pPin)
{
  signed char vPin = -1;

  for (byte i = 0; i < 18; i++)
  {
    if (LEDData.Pins[i] == pPin)
    {
      vPin = i;
    }
  }

  return vPin;
}


void BlinkTest()
{
  unsigned long ul_Entered = micros();
  const unsigned long ul_Timer = 400UL;

  do
  {
    if (timetoprint)
    {
      timetoprint = false;

      if (slot < ltestno)
      {
        Serial.print(ltestno);
      }
      else
      {
        //Serial.print('a');
      }
    }

  } while ((micros() - ul_Entered) < ul_Timer);

  slot++;
  if (slot > ltestno + 3)
  {
    slot = 0;
  }
}


void LighSegment(byte inc)
{
  static signed char segmentpin = -1;
  static signed char digitpin = -1;

  AllPinsInput();

  if (inc)
  {
    IncSegment(1);
    segmentpin = -1;
    digitpin = -1;
  }

  if (segmentpin == -1)
  {
    do
    {
      segmentpin = FindPin(IncSegment());

      if (segmentpin == -1)
      {
        IncSegment(1);
      }

    } while (segmentpin == -1);
    segmentpin++;
    timetoprint = true;
  }

  if (digitpin == -1)
  {
    do
    {
      digitpin = FindPin(IncDigit());

      if (digitpin == -1)
      {
        IncDigit(1);
      }

    } while (digitpin == -1);
    digitpin++;
  }

  SetPin(segmentpin, (LEDData.IsCC == 1) ?  1 : 0);
  SetPin(digitpin, (LEDData.IsCC == 1) ?  0 : 1);

  BlinkTest(); // delays for 400 us

  AllPinsInput();
  if (LEDData.IsCC)
  {
    AllPinsLow();
  }
  else
  {
    AllPinsHigh();
  }
}

