# Programmable Seven & Sixteen Segment LED Tester

![Sixteen Segment LED Tester](https://cdn.hackaday.io/images/8274681690742840319.jpg)

## Name
A Programmable Seven and Sixteen Segment LED Tester 

## Description
A device built with an Arduino Nano and a ZIF socket that can be programmed to test any DIP (dual inline packege) or SIP (single inline package) LED display.

## Project Homepage
See this project at [hackaday.io/project/192160-programmable-seven-sixteen-segment-led-tester](https://hackaday.io/project/192160-programmable-seven-sixteen-segment-led-tester)

Get the PCB files at OshPark.com [oshpark.com/shared_projects/4dCs8dWh](https://oshpark.com/shared_projects/4dCs8dWh)

This project uses the following library [github.com/mikaelpatel/Arduino-GPIO](https://github.com/mikaelpatel/Arduino-GPIO)

## Our other projects
[arduinoenigma.blogspot.com](https://arduinoenigma.blogspot.com)

## Project Details

An Arduino Nano is connected to a ZIF socket and is programmed with test sequences ranging from simple two pin LEDs to 16 segment displays and multiple digit displays. The sequences are changed with a double reset of the Arduino Nano.

An exercise in minimalism. To maximize the amount of pins available to test LEDs, no extra buttons are used to change the test sequences. The current sequence is changed by pressing reset twice within 5 seconds. The selected sequence is saved to EEPROM so next time it is powered up, it defaults to the last sequence used.

To simplify the design, it uses no current limiting resistors, brightness is controlled by PWM. Extensive testing has proved that neither the Arduino or the LED is damaged.

New devices can be added to the sketch by simply defining a new device and describing what each pin (pin 1 is segmentA, pin 2 is CommonCathode1). The code uses that description to sequence through each segment and each digit.

