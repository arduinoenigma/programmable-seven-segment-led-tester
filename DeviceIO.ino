//
// A0 = D14
// A1 = D15
// A2 = D16
// A3 = D17
// A4 = D18
// A5 = D19
// A6 = D20
// A7 = D21
//

GPIO<BOARD::D2>  LPIN18;
GPIO<BOARD::D3>  LPIN17;
GPIO<BOARD::D4>  LPIN16;
GPIO<BOARD::D5>  LPIN15;
GPIO<BOARD::D6>  LPIN14;
GPIO<BOARD::D7>  LPIN13;
GPIO<BOARD::D8>  LPIN12;
GPIO<BOARD::D9>  LPIN10;
GPIO<BOARD::D10> LPIN2;
GPIO<BOARD::D11> LPIN4;
GPIO<BOARD::D12> LPIN6;
GPIO<BOARD::D13> LPIN9;
GPIO<BOARD::D14> LPIN8;   //A0
GPIO<BOARD::D15> LPIN7;   //A1
GPIO<BOARD::D16> LPIN5;   //A2
GPIO<BOARD::D17> LPIN3;   //A3
GPIO<BOARD::D18> LPIN1;   //A4
GPIO<BOARD::D19> LPIN11;  //A5


void AllPinsInput()
{
  //Serial.println(F("all input"));

  LPIN1.input();
  LPIN2.input();
  LPIN3.input();
  LPIN4.input();
  LPIN5.input();
  LPIN6.input();
  LPIN7.input();
  LPIN8.input();
  LPIN9.input();
  LPIN10.input();
  LPIN11.input();
  LPIN12.input();
  LPIN13.input();
  LPIN14.input();
  LPIN15.input();
  LPIN16.input();
  LPIN17.input();
  LPIN18.input();
}

void AllPinsHigh()
{
  LPIN1.high();
  LPIN2.high();
  LPIN3.high();
  LPIN4.high();
  LPIN5.high();
  LPIN6.high();
  LPIN7.high();
  LPIN8.high();
  LPIN9.high();
  LPIN10.high();
  LPIN11.high();
  LPIN12.high();
  LPIN13.high();
  LPIN14.high();
  LPIN15.high();
  LPIN16.high();
  LPIN17.high();
  LPIN18.high();
}

void AllPinsLow()
{
  LPIN1.low();
  LPIN2.low();
  LPIN3.low();
  LPIN4.low();
  LPIN5.low();
  LPIN6.low();
  LPIN7.low();
  LPIN8.low();
  LPIN9.low();
  LPIN10.low();
  LPIN11.low();
  LPIN12.low();
  LPIN13.low();
  LPIN14.low();
  LPIN15.low();
  LPIN16.low();
  LPIN17.low();
  LPIN18.low();
}

void SetPin(byte pin, byte value)
{
  if (value)
  {
    SetPinHigh(pin);
  }
  else
  {
    SetPinLow(pin);
  }
  SetPinOutput(pin);
}

void SetPinOutput(byte pin)
{
  //Serial.print(F("out "));
  //Serial.println(pin);

  switch (pin)
  {
    case 1:
      LPIN1.output();
      break;
    case 2:
      LPIN2.output();
      break;
    case 3:
      LPIN3.output();
      break;
    case 4:
      LPIN4.output();
      break;
    case 5:
      LPIN5.output();
      break;
    case 6:
      LPIN6.output();
      break;
    case 7:
      LPIN7.output();
      break;
    case 8:
      LPIN8.output();
      break;
    case 9:
      LPIN9.output();
      break;
    case 10:
      LPIN10.output();
      break;
    case 11:
      LPIN11.output();
      break;
    case 12:
      LPIN12.output();
      break;
    case 13:
      LPIN13.output();
      break;
    case 14:
      LPIN14.output();
      break;
    case 15:
      LPIN15.output();
      break;
    case 16:
      LPIN16.output();
      break;
    case 17:
      LPIN17.output();
      break;
    case 18:
      LPIN18.output();
      break;
  }
}

void SetPinInput(byte pin)
{
  //Serial.print(F("input "));
  //Serial.println(pin);

  switch (pin)
  {
    case 1:
      LPIN1.input();
      break;
    case 2:
      LPIN2.input();
      break;
    case 3:
      LPIN3.input();
      break;
    case 4:
      LPIN4.input();
      break;
    case 5:
      LPIN5.input();
      break;
    case 6:
      LPIN6.input();
      break;
    case 7:
      LPIN7.input();
      break;
    case 8:
      LPIN8.input();
      break;
    case 9:
      LPIN9.input();
      break;
    case 10:
      LPIN10.input();
      break;
    case 11:
      LPIN11.input();
      break;
    case 12:
      LPIN12.input();
      break;
    case 13:
      LPIN13.input();
      break;
    case 14:
      LPIN14.input();
      break;
    case 15:
      LPIN15.input();
      break;
    case 16:
      LPIN16.input();
      break;
    case 17:
      LPIN17.input();
      break;
    case 18:
      LPIN18.input();
      break;
  }
}

void SetPinHigh(byte pin)
{
  //Serial.print(F("high "));
  //Serial.println(pin);

  switch (pin)
  {
    case 1:
      LPIN1.high();
      break;
    case 2:
      LPIN2.high();
      break;
    case 3:
      LPIN3.high();
      break;
    case 4:
      LPIN4.high();
      break;
    case 5:
      LPIN5.high();
      break;
    case 6:
      LPIN6.high();
      break;
    case 7:
      LPIN7.high();
      break;
    case 8:
      LPIN8.high();
      break;
    case 9:
      LPIN9.high();
      break;
    case 10:
      LPIN10.high();
      break;
    case 11:
      LPIN11.high();
      break;
    case 12:
      LPIN12.high();
      break;
    case 13:
      LPIN13.high();
      break;
    case 14:
      LPIN14.high();
      break;
    case 15:
      LPIN15.high();
      break;
    case 16:
      LPIN16.high();
      break;
    case 17:
      LPIN17.high();
      break;
    case 18:
      LPIN18.high();
      break;
  }
}

void SetPinLow(byte pin)
{
  //Serial.print(F("low "));
  //Serial.println(pin);

  switch (pin)
  {
    case 1:
      LPIN1.low();
      break;
    case 2:
      LPIN2.low();
      break;
    case 3:
      LPIN3.low();
      break;
    case 4:
      LPIN4.low();
      break;
    case 5:
      LPIN5.low();
      break;
    case 6:
      LPIN6.low();
      break;
    case 7:
      LPIN7.low();
      break;
    case 8:
      LPIN8.low();
      break;
    case 9:
      LPIN9.low();
      break;
    case 10:
      LPIN10.low();
      break;
    case 11:
      LPIN11.low();
      break;
    case 12:
      LPIN12.low();
      break;
    case 13:
      LPIN13.low();
      break;
    case 14:
      LPIN14.low();
      break;
    case 15:
      LPIN15.low();
      break;
    case 16:
      LPIN16.low();
      break;
    case 17:
      LPIN17.low();
      break;
    case 18:
      LPIN18.low();
      break;
  }
}


